from django.urls import path
from . import views

urlpatterns = [
    path('',views.BlogListView.as_view(), name = 'blogs'),
    path('<int:pk>/<slug:slug>',views.BlogDetailView.as_view(), name = 'post_detail'),
]
