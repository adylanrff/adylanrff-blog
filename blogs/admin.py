from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget

from .models import BlogPost
# Register your models here.


class PostAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget(config_name='custom'))
    class Meta:
        model = BlogPost
        fields = '__all__'

class BlogAdmin(admin.ModelAdmin):
    form = PostAdminForm
    prepopulated_fields = {'slug': ('title',),}
    list_display = ['title','date','content','tag_list']

    date_hierarchy='date'

    def get_queryset(self, request):
        return super(BlogAdmin, self).get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())


admin.site.register(BlogPost, BlogAdmin)
