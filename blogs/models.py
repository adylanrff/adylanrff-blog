from django.db import models
from taggit.managers import TaggableManager

# Create your models here.
class BlogPost(models.Model):

    class Meta:
        ordering = ["-date"]

    title = models.CharField(max_length = 120)
    content = models.TextField()
    slug = models.SlugField(max_length=140, unique = True)
    date = models.DateTimeField(auto_now_add=True)
    tags = TaggableManager()



    def get_absolute_url(self):
        return reverse('blog')

    def __str__(self):
        return self.title
