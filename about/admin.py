from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget

from .models import About

# Register your models here.
class BlogAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget(config_name='custom'))
    class Meta:
        model = About
        fields = '__all__'

class AboutAdmin(admin.ModelAdmin):
    form = BlogAdminForm


admin.site.register(About, AboutAdmin)
