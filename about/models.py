from django.db import models

# Create your models here.
class About(models.Model):
    content = models.TextField()

    def get_absolute_url(self):
        return reverse('about')

    def __str__(self):
        return 'About me'
