from django.views.generic import ListView
from django.shortcuts import render
from .models import About
# Create your views here.
class AboutView(ListView):
    model = About
    template_name = 'about.html'
